import { Typography, Box } from "@mui/material";
import React from "react";
import EditIcon from "../decorative/EditIcon";
import Link from "../wrappers/Link";

interface NavbarProps {}

const Navbar: React.FC<NavbarProps> = ({}) => {
  return (
    <>
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        bgcolor="primary.main"
        padding="20px"
        borderRadius="7px"
        margin="20px 50px"
      >
        <Link href="/reviews">
          <Typography
            align="center"
            component="h2"
            variant="h5"
            noWrap
            color="white"
            sx={{ flex: 1 }}
          >
            EcoPortal
          </Typography>
        </Link>
        <Link
          href="/reviews/new"
          bgcolor="secondary.main"
          borderRadius="7px"
          padding="10px"
          display="flex"
          alignItems="center"
          sx={{
            color: "black",
            textDecoration: "none",
          }}
        >
          <EditIcon />
          Create a review
        </Link>
      </Box>
    </>
  );
};

export default Navbar;
