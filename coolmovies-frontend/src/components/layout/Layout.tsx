import { Box } from "@mui/material";
import React from "react";
import Navbar from "./Navbar";

interface LayoutProps {}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <Box>
      <Navbar />
      <Box padding="50px">{children}</Box>
    </Box>
  );
};

export default Layout;
