import React from "react";
import NextLink from "next/link";
import { Link as MUILink, LinkProps as MUILinkProps } from "@mui/material";

interface LinkProps extends MUILinkProps {}

const Link: React.FC<LinkProps> = ({ href, ...props }) => {
  if (!href) {
    // If we don't have an href, we don't want to break the UI
    console.error("Link: href is required");
    href = "/";
  }
  return (
    <>
      <NextLink href={href} passHref>
        <MUILink {...props} />
      </NextLink>
    </>
  );
};

export default Link;
