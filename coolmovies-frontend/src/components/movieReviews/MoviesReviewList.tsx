import { Box, Typography } from "@mui/material";
import React from "react";
import { useGetReviewsQuery } from "../../graphql";
import Loader from "../decorative/Loader";
import MovieReviewCard from "./MovieReviewCard";

interface MoviesListProps {}

const MoviesReviewList: React.FC<MoviesListProps> = ({}) => {
  const { data, loading } = useGetReviewsQuery();
  if (loading) return <Loader />;

  return (
    <Box display="flex" flexDirection="column" gap="20px">
      {data?.allMovieReviews?.nodes
        .filter((node): node is NonNullable<typeof node> => !!node)
        .map((review) => (
          <MovieReviewCard
            key={review.id}
            rating={review.rating || 0}
            title={review.title}
            body={review.body || ""}
            author={review.userByUserReviewerId?.name || "Unknown"}
            id={review.id}
          />
        ))}
    </Box>
  );
};

export default MoviesReviewList;
