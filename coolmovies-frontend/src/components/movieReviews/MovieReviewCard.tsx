import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import Link from "../wrappers/Link";
import Stars from "../decorative/Stars";

interface MovieReviewCardProps {
  rating: number;
  title: string;
  body: string;
  author: string;
  id: number;
}

const MovieReviewCard: React.FC<MovieReviewCardProps> = ({
  rating,
  title,
  body,
  author,
  id,
}) => {
  return (
    <Link
      width="100%"
      padding="20px"
      borderRadius="8px"
      border="1px solid"
      borderColor="secondary.main"
      sx={{ textDecoration: "none", color: "unset" }}
      href={`/reviews/${id}`}
    >
      <Box display="flex" justifyContent="space-between" width="100%">
        <Box>
          <Typography variant="h3" display="inline-block">
            {title}
          </Typography>
          <Typography
            variant="subtitle1"
            display="inline-block"
            marginLeft="10px"
          >
            - {author}
          </Typography>
        </Box>

        <Stars rating={rating} />
      </Box>
      <Typography>{body}</Typography>
    </Link>
  );
};

export default MovieReviewCard;
