import React from "react";
import MovieReviewForm from "./MovieReviewForm";
import {
  act,
  cleanup,
  fireEvent,
  render,
  screen,
} from "@testing-library/react";
import userEvent from "@testing-library/user-event";

const handleSubmit = jest.fn();
const handleChange = jest.fn();

const mockUsersAnsMovies = {
  data: {
    allMovies: { nodes: [{ title: "lorem", id: "abcd-123" }] },
    allUsers: { nodes: [{ name: "ipsum", id: "efgh-456" }] },
  },
  loading: false,
};

jest.mock("../../graphql", () => ({
  useGetUsersAndMoviesQuery: jest.fn(() => mockUsersAnsMovies),
}));

describe("<MovieReviewForm />", () => {
  beforeEach(() => {
    render(<MovieReviewForm onSubmit={handleSubmit} onChange={handleChange} />);
  });
  afterEach(() => {
    jest.clearAllMocks();
    cleanup();
  });
  it("should call handleSubmit when form is submitted", () => {
    const submit = screen.getByTestId("submit");
    act(() => {
      fireEvent.click(submit);
    });
    expect(handleSubmit).toHaveBeenCalledTimes(1);
  });

  it("should call submit with the new title when the title is changed", async () => {
    const title = screen.getByTestId("title");

    await act(() => userEvent.type(title, "new title"));

    expect(handleChange).toHaveBeenCalledWith(
      expect.objectContaining({ title: "new title" })
    );
  });

  it("should call submit with all edited fields when submit is clicked", async () => {
    const title = screen.getByTestId("title");
    const body = screen.getByTestId("body");

    await act(() => userEvent.type(title, "new title"));
    await act(() => userEvent.type(body, "super nice movie"));

    const submit = screen.getByTestId("submit");
    act(() => {
      fireEvent.click(submit);
    });

    expect(handleSubmit).toHaveBeenCalledWith(
      expect.objectContaining({
        title: "new title",
        body: "super nice movie",
      })
    );
  });
});
