import { LoadingButton } from "@mui/lab";
import {
  Box,
  FormControl,
  Input,
  InputLabel,
  MenuItem,
  Rating,
  Select,
} from "@mui/material";
import React, { FormEventHandler, useEffect, useState } from "react";
import {
  Movie,
  MovieReviewInput,
  useGetUsersAndMoviesQuery,
  User,
} from "../../graphql";
import Loader from "../decorative/Loader";

interface MovieReviewFormProps {
  onChange?: (movieReviewInput: MovieReviewInput) => void;
  onSubmit?: (movieReviewInput: MovieReviewInput) => void;
  isLoading?: boolean;
  isSubmitting?: boolean;
  initialValues?: MovieReviewInput;
}

const MovieReviewForm: React.FC<MovieReviewFormProps> = ({
  onChange,
  onSubmit,
  isLoading,
  isSubmitting,
  initialValues,
}) => {
  const [form, setForm] = useState<MovieReviewInput>(
    initialValues || {
      movieId: "",
      title: "",
      userReviewerId: "",
      body: "",
      rating: 0,
    }
  );
  const { data, loading } = useGetUsersAndMoviesQuery();
  const movies = data?.allMovies?.nodes.filter(
    (n): n is NonNullable<Pick<Movie, "id" | "title">> => !!n
  );
  const users = data?.allUsers?.nodes.filter(
    (n): n is NonNullable<Pick<User, "id" | "name">> => !!n
  );
  useEffect(() => {
    if (!loading && movies && users) {
      setForm((form) => ({
        ...form,
        movieId: form.movieId || movies[0].id,
        userReviewerId: form.userReviewerId || users[0].id,
      }));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, loading]);

  useEffect(() => {
    onChange?.(form);
  }, [form, onChange]);

  if (loading || isLoading) return <Loader />;

  const handleSubmit: FormEventHandler = (e) => {
    e.preventDefault();
    onSubmit?.(form);
  };

  return (
    <Box
      component="form"
      width="100%"
      display="flex"
      flexDirection="column"
      gap="20px"
      onSubmit={handleSubmit}
    >
      <Box display="grid" gridTemplateColumns="5fr 1fr" gap="20px">
        <FormControl fullWidth>
          <InputLabel htmlFor="title">Title</InputLabel>
          <Input
            data-testid="title"
            id="title"
            value={form.title}
            onChange={(e) => setForm({ ...form, title: e.target.value })}
          />
        </FormControl>
        <Rating
          data-testid="rating"
          value={form.rating}
          onChange={(_e, rating) => {
            setForm({ ...form, rating });
          }}
        />
      </Box>
      <Box display="grid" gridTemplateColumns="repeat(2, 1fr)" gap="20px">
        <FormControl fullWidth>
          <InputLabel htmlFor="movie">Movie</InputLabel>
          <Select
            id="movie"
            data-testid="movie"
            value={form.movieId}
            label="Movie"
            onChange={(e) => setForm({ ...form, movieId: e.target.value })}
          >
            {movies?.map((movie) => (
              <MenuItem key={movie.id} value={movie.id}>
                {movie.title}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl fullWidth>
          <InputLabel htmlFor="user">User</InputLabel>
          <Select
            id="user"
            data-testid="user"
            value={form.userReviewerId}
            label="user"
            onChange={(e) =>
              setForm({ ...form, userReviewerId: e.target.value })
            }
          >
            {users?.map((user) => (
              <MenuItem key={user.id} value={user.id}>
                {user.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
      <FormControl fullWidth>
        <InputLabel htmlFor="body">Review</InputLabel>
        <Input
          multiline
          data-testid="body"
          minRows={3}
          id="body"
          value={form.body}
          onChange={(e) => setForm({ ...form, body: e.target.value })}
        />
      </FormControl>
      <LoadingButton loading={isSubmitting} type="submit" data-testid="submit">
        Publish review
      </LoadingButton>
    </Box>
  );
};

export default MovieReviewForm;
