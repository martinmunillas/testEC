import { Star, StarBorder } from "@mui/icons-material";
import { Box } from "@mui/system";
import React from "react";

interface StarsProps {
  rating: number;
}

const Stars: React.FC<StarsProps> = ({ rating }) => {
  return (
    <Box color="secondary.main">
      {Array.from({ length: 5 }, (_, i) =>
        i < rating ? <Star key={i} /> : <StarBorder key={i} />
      )}
    </Box>
  );
};

export default Stars;
