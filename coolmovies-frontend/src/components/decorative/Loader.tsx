import { Typography } from "@mui/material";
import React from "react";

interface LoaderProps {}

const Loader: React.FC<LoaderProps> = ({}) => {
  return <Typography>Loading...</Typography>;
};

export default Loader;
