import { Box } from "@mui/material";
import React from "react";

interface EditIconProps {
  size?: string;
  color?: string;
}

const EditIcon: React.FC<EditIconProps> = ({
  size = "1.5em",
  color = "#333",
}) => {
  return (
    <Box
      component="img"
      height={size}
      width={size}
      color={color}
      role="presentation"
      src="/edit.svg"
    />
  );
};

export default EditIcon;
