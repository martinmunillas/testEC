import "../styles/globals.css";
import type { AppProps } from "next/app";
import React, { FC } from "react";
import Head from "next/head";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import { createTheme, ThemeProvider } from "@mui/material";

const client = new ApolloClient({
  cache: new InMemoryCache(),
  uri: "/graphql",
});

const App: FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <>
      <Head>
        <title>Coolmovies Frontend</title>
        <meta charSet="UTF-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <ThemeProvider
        theme={createTheme({
          palette: {
            primary: {
              main: "#d62828",
              light: "#ff7961",
              dark: "#8e0000",
            },
            secondary: {
              main: "#F4B743",
              dark: "#A88A52",
              light: "#F5D8A0",
            },
          },
        })}
      >
        <ApolloProvider client={client}>
          <Component {...pageProps} />
        </ApolloProvider>
      </ThemeProvider>
    </>
  );
};

export default App;
