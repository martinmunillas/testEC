import { Typography } from "@mui/material";
import { NextPage } from "next";
import React from "react";
import Layout from "../components/layout/Layout";

interface NotFoundProps {
  message?: string;
}

const NotFound: NextPage<NotFoundProps> = ({ message }) => {
  return (
    <Layout>
      <Typography variant="h1">404</Typography>
      <Typography variant="h2">{message || "Page not found"}</Typography>
    </Layout>
  );
};

export default NotFound;
