import { Box, Typography } from "@mui/material";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React from "react";
import Layout from "../../../components/layout/Layout";
import Loader from "../../../components/decorative/Loader";
import Stars from "../../../components/decorative/Stars";
import { useGetReviewQuery } from "../../../graphql";
import NotFound from "../../404";
import Link from "../../../components/wrappers/Link";
import EditIcon from "../../../components/decorative/EditIcon";

const Review: NextPage = ({}) => {
  const router = useRouter();
  const { data, loading } = useGetReviewQuery({
    variables: { id: router.query.id },
  });
  if (loading)
    return (
      <Layout>
        <Loader />
      </Layout>
    );
  if (!data?.movieReviewById) return <NotFound />;

  const comments = data?.movieReviewById.commentsByMovieReviewId?.nodes.filter(
    (node): node is NonNullable<typeof node> => !!node
  );

  return (
    <Layout>
      <Box display="grid" gridTemplateColumns="5fr 1fr">
        <Box>
          <Typography variant="h1">{data?.movieReviewById.title} </Typography>
          <Box display="flex">
            <Stars rating={data.movieReviewById.rating || 0} />
            <Typography paddingLeft="10px">
              - {data.movieReviewById.userByUserReviewerId?.name}
            </Typography>
          </Box>
          <Typography variant="body1">{data?.movieReviewById.body}</Typography>
        </Box>
        <Box>
          <Box
            component="img"
            width="250px"
            src={data.movieReviewById.movieByMovieId?.imgUrl}
            alt={data.movieReviewById.movieByMovieId?.title}
          />
          <Link
            href={`/reviews/${router.query.id}/edit`}
            bgcolor="secondary.main"
            borderRadius="7px"
            padding="10px"
            display="flex"
            alignItems="center"
            gap="20px"
            sx={{
              color: "black",
              textDecoration: "none",
            }}
          >
            <EditIcon />
            Edit Review
          </Link>
        </Box>
      </Box>
      <Typography variant="h2">Comments</Typography>
      <Box display="flex" flexDirection="column" gap="20px">
        {comments.length ? (
          comments.map((comment) => (
            <Box key={comment.id}>
              <Typography variant="body1">{comment.body}</Typography>
              <Typography variant="subtitle1">
                - {comment.userByUserId?.name}
              </Typography>
            </Box>
          ))
        ) : (
          <Typography variant="body1">No comments yet</Typography>
        )}
      </Box>
    </Layout>
  );
};

export default Review;
