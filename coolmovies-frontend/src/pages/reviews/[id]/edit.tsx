import { NextPage } from "next";
import { useRouter } from "next/router";
import React from "react";
import {
  useGetReviewQuery,
  useGetUsersAndMoviesQuery,
  useUpdateReviewMutation,
} from "../../../graphql";
import Layout from "../../../components/layout/Layout";
import MovieReviewForm from "../../../components/movieReviews/MovieReviewForm";
import { MovieReviewInput } from "../../../graphql";
import { Typography } from "@mui/material";
import Link from "../../../components/wrappers/Link";
import { ArrowBack } from "@mui/icons-material";

const Review: NextPage = ({}) => {
  const [update, { loading: updating }] = useUpdateReviewMutation();
  const router = useRouter();
  const { loading: loadingOptions } = useGetUsersAndMoviesQuery();
  const { data, loading } = useGetReviewQuery({
    variables: { id: router.query.id },
  });

  const review = data?.movieReviewById;

  if (!review) {
    return (
      <Layout>
        <Typography component="h1">Review not found</Typography>
      </Layout>
    );
  }

  const handleSubmit = async (form: MovieReviewInput) => {
    await update({
      variables: {
        input: {
          movieReviewPatch: form,
          nodeId: review.nodeId,
        },
      },
    });
    router.push(`/reviews/${router.query.id}`);
  };
  return (
    <Layout>
      <MovieReviewForm
        isLoading={loadingOptions || loading}
        isSubmitting={updating}
        onSubmit={handleSubmit}
        initialValues={{
          movieId: review.movieId,
          title: review.title,
          userReviewerId: review.userReviewerId,
          body: review.body,
          rating: review.rating,
        }}
      />
      <Link
        href={`/reviews/${router.query.id}`}
        bgcolor="secondary.main"
        borderRadius="7px"
        padding="10px"
        display="flex"
        alignItems="center"
        width="fit-content"
        gap="5px"
        marginTop="10px"
        sx={{
          color: "black",
          textDecoration: "none",
        }}
      >
        <ArrowBack /> Cancel
      </Link>
    </Layout>
  );
};

export default Review;
