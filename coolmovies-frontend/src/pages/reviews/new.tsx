import { Box, Typography } from "@mui/material";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React from "react";
import Layout from "../../components/layout/Layout";
import MovieReviewForm from "../../components/movieReviews/MovieReviewForm";
import {
  GetReviewsDocument,
  MovieReviewInput,
  useCreateReviewMutation,
} from "../../graphql";

const Review: NextPage = ({}) => {
  const [create, { loading }] = useCreateReviewMutation();
  const router = useRouter();

  const handleSubmit = async (form: MovieReviewInput) => {
    const { data } = await create({
      variables: {
        input: {
          movieReview: form,
        },
      },
      refetchQueries: [{ query: GetReviewsDocument }],
    });
    router.push(`/reviews/${data?.createMovieReview?.movieReview?.id}`);
  };
  return (
    <Layout>
      <MovieReviewForm isLoading={loading} onSubmit={handleSubmit} />
    </Layout>
  );
};

export default Review;
