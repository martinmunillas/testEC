import type { NextPage } from "next";
import Layout from "../../components/layout/Layout";
import MoviesReviewList from "../../components/movieReviews/MoviesReviewList";

const Home: NextPage = () => {
  return (
    <Layout>
      <MoviesReviewList />
    </Layout>
  );
};

export default Home;
