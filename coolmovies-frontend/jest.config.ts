import nextJest from "next/jest";
import type { InitialOptionsTsJest } from "ts-jest/dist/types";

const createJestConfig = nextJest({
  dir: "./",
});

const config: InitialOptionsTsJest = {
  preset: "ts-jest",
  testEnvironment: "jsdom",
};

module.exports = createJestConfig(config);
