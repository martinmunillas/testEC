/** @type {import('next').NextConfig} */
module.exports = {
  async rewrites() {
    return [
      {
        source: "/graphql",
        destination: process.env.SERVER_URL || "http://localhost:5001/graphql",
      },
      {
        source: "/",
        destination: "/reviews",
      },
    ];
  },
  reactStrictMode: true,
};
